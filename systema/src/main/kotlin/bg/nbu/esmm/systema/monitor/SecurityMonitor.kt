package bg.nbu.esmm.systema.monitor

import Commander
import EventManagerInterface
import MessageWindow
import bg.nbu.esmm.systema.Consts

object Global {
    var systemArmed = false
}

fun main(args: Array<String>) {
    val ip = args.getOrNull(0)
    val em = if (ip == null) EventManagerInterface() else EventManagerInterface(ip)
    val (console, armDisarmCommander) = drawUi()
    getEventMessages(em, console, armDisarmCommander).forEach { msg ->
        if (Global.systemArmed) {
            val alertType = when {
                msg.startsWith(Consts.WINDOW_BREAK_ALERT, ignoreCase = true) -> "Window break"
                msg.startsWith(Consts.DOOR_BREAK_ALERT, ignoreCase = true) -> "Door break"
                msg.startsWith(Consts.MOTION_DETECTION_ALERT, ignoreCase = true) -> "Motion detection"
                else -> null
            }
            val state = if (msg.last() == '1') {
                "ON"
            } else {
                "OFF"
            }
            alertType?.let { alert ->
                console.WriteMessage("$alert alert is $state")
            }
        }
    }
}

fun getEventMessages(em: EventManagerInterface, console: MessageWindow, armDisarmCommander: Commander): Sequence<String> {
    var listening = true
    return sequence {
        while (listening) {
            val eventQueue = try {
                em.GetEventQueue()
            } catch (e: Exception) {
                console.WriteMessage("Error getting event queue: $e"); null
            }
            eventQueue?.let { queue ->
                repeat(queue.GetSize()) {
                    val event = queue.GetEvent()
                    val id = event.GetEventId()
                    if (id == Consts.END_EVENT_ID) {
                        try {
                            em.UnRegister()
                        } catch (e: Exception) {
                            console.WriteMessage("Error unregistering: $e")
                        }
                        console.WriteMessage("\n\nSimulation stopped\n\n")
                        armDisarmCommander.dispose()
                        listening = false
                    } else {
                        if (id == Consts.SECURITY_EVENT_CONFIRMED_ID) {
                            yield(event.GetMessage())
                        }
                        Thread.sleep(Consts.DELAY)
                    }
                }
            }
        }
    }
}

fun drawUi(): Pair<MessageWindow, Commander> {
    val console = MessageWindow("Security Monitor Console", 0.0f, 0.40f)
    val armDisarmCommander = Commander("Arm/Disarm security system", Pair(200, 80), Pair(100, 100), arrayOf(
            Pair("ARM", { _ -> Global.systemArmed = true }),
            Pair("DISARM", { _ -> Global.systemArmed = false })
    ))
    return Pair(console, armDisarmCommander)
}