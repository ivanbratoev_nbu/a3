package bg.nbu.esmm.systema

object Consts {
    const val WINDOW_BREAK_ALERT = "WB"
    const val DOOR_BREAK_ALERT = "DB"
    const val MOTION_DETECTION_ALERT = "MD"
    const val DELAY = 250L
    const val SECURITY_EVENT_ID = 10
    const val SECURITY_EVENT_CONFIRMED_ID = -10
    const val END_EVENT_ID = 99
}