package bg.nbu.esmm.systemb.security.controller

import Event
import EventManagerInterface
import Indicator
import MessageWindow
import bg.nbu.esmm.systemb.Consts


fun main(args: Array<String>) {
    val ip = args.getOrNull(0)
    val em = if (ip == null) EventManagerInterface() else EventManagerInterface(ip)

    val (console, indicators) = drawUi()
    val (windowBreakIndicator, doorBreakIndicator, motionDetectionIndicator) = indicators

    getEventMessageSequence(em, console, indicators).forEach { message ->
        val newState = if (message.last() == '1') {
            3
        } else {
            0
        }
        val newStateStr = if (newState != 0) {
            "ON"
        } else {
            "OFF"
        }
        when {
            message.startsWith(Consts.WINDOW_BREAK_ALERT, ignoreCase = true) -> {
                val str = "Window break alert $newStateStr"
                console.WriteMessage(str)
                windowBreakIndicator.SetLampColorAndMessage(str, newState)
            }
            message.startsWith(Consts.DOOR_BREAK_ALERT, ignoreCase = true) -> {
                val str = "Door break alert $newStateStr"
                console.WriteMessage(str)
                doorBreakIndicator.SetLampColorAndMessage(str, newState)
            }
            message.startsWith(Consts.MOTION_DETECTION_ALERT, ignoreCase = true) -> {
                val str = "Motion detection alert $newStateStr"
                console.WriteMessage(str)
                motionDetectionIndicator.SetLampColorAndMessage(str, newState)
            }
        }
        confirmMessage(em, message)
    }

    println("Registered with the event manager.")
}

fun confirmMessage(em: EventManagerInterface, message: String) {
    try {
        em.SendEvent(Event(Consts.SECURITY_EVENT_CONFIRMED_ID, message))
    } catch (e: Exception) {
        println("Error Confirming Message:: $e")
    }
}


fun getEventMessageSequence(em: EventManagerInterface, console: MessageWindow, indicators: Triple<Indicator, Indicator, Indicator>): Sequence<String> {
    return sequence {

        var listening = true
        while (listening) {
            val eventQueue = try {
                em.GetEventQueue()
            } catch (e: Exception) {
                console.WriteMessage("Error getting event queue: $e"); null
            }
            eventQueue?.let { queue ->
                repeat(queue.GetSize()) {
                    val event = queue.GetEvent()
                    val id = event.GetEventId()
                    if (id == Consts.END_EVENT_ID) {
                        try {
                            em.UnRegister()
                        } catch (e: Exception) {
                            console.WriteMessage("Error unregistering: $e")
                        }
                        console.WriteMessage("\n\nSimulation stopped\n\n")
                        val (wbi, dbi, mdi) = indicators
                        wbi.dispose()
                        dbi.dispose()
                        mdi.dispose()
                        listening = false
                    } else {
                        if (id == Consts.SECURITY_EVENT_ID) {
                            yield(event.GetMessage())
                        }
                    }
                }
            }
            Thread.sleep(Consts.DELAY)
        }
    }
}

fun drawUi(): Pair<MessageWindow, Triple<Indicator, Indicator, Indicator>> {
    val winPosX = 0.0f        //This is the X position of the message window in terms
    //of a percentage of the screen height
    val winPosY = 0.60f        //This is the Y position of the message window in terms
    //of a percentage of the screen height

    val console = MessageWindow("Security Controller Status Console", winPosX, winPosY)

    // Now we put the indicators directly under the security status and control panel

    val windowBreakIndicator = Indicator("Window break alert OFF", console.GetX(), console.GetY() + console.Height())
    val doorBreakIndicator = Indicator("Door break alert OFF", console.GetX() + (windowBreakIndicator.Width() * 2), console.GetY() + console.Height())
    val motionDetectionIndicator = Indicator("Motion detection alert OFF", doorBreakIndicator.GetX() + (doorBreakIndicator.Width() * 2), console.GetY() + console.Height())
    return Pair(console, Triple(windowBreakIndicator, doorBreakIndicator, motionDetectionIndicator))
}