package bg.nbu.esmm.systemb

object Consts {
    const val WINDOW_BREAK_ALERT = "WB"
    const val DOOR_BREAK_ALERT = "DB"
    const val MOTION_DETECTION_ALERT = "MD"
    const val FIRE_ALARM = "FA"
    const val START_SPRINKLER = "SS"
    const val FIRE_EXTINGUISHED = "FE"
    const val DELAY = 250L
    const val FIRE_EVENT_ID = 15
    const val FIRE_EVENT_CONFIRMED_ID = -15
    const val SECURITY_EVENT_ID = 10
    const val SECURITY_EVENT_CONFIRMED_ID = -10
    const val END_EVENT_ID = 99
}