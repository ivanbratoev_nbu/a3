package bg.nbu.esmm.systemb.fire.sensor

import Commander
import Event
import EventManagerInterface
import bg.nbu.esmm.systemb.Consts

fun main(args: Array<String>) {
    val ip = args.getOrNull(0)
    val em = if (ip == null) EventManagerInterface() else EventManagerInterface(ip)

    drawUi(em)
}

fun drawUi(em: EventManagerInterface) {
    Commander("Trigger fire", Pair(180, 80), Pair(400, 50), arrayOf(
            Pair("Burn everything", { _ -> postEvent(em, Consts.FIRE_ALARM) })
    ))
}

fun postEvent(em: EventManagerInterface, alertType: String) {
    em.SendEvent(Event(Consts.FIRE_EVENT_ID, alertType))
}
