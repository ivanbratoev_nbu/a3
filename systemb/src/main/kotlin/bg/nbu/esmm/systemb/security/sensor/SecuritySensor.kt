package bg.nbu.esmm.systemb.security.sensor

import Commander
import Event
import EventManagerInterface
import bg.nbu.esmm.systemb.Consts

fun main(args: Array<String>) {
    val ip = args.getOrNull(0)
    val em = if (ip == null) EventManagerInterface() else EventManagerInterface(ip)

    drawUi(em)
}

fun drawUi(em: EventManagerInterface) {
    Commander("Trigger security events", Pair(300, 200), Pair(100, 200), arrayOf(
            Pair("Window break on", { _ -> postEvent(em, Consts.WINDOW_BREAK_ALERT, 1) }),
            Pair("Window break off", { _ -> postEvent(em, Consts.WINDOW_BREAK_ALERT, 0) }),
            Pair("Door break on", { _ -> postEvent(em, Consts.DOOR_BREAK_ALERT, 1) }),
            Pair("Door break off", { _ -> postEvent(em, Consts.DOOR_BREAK_ALERT, 0) }),
            Pair("Motion detection on", { _ -> postEvent(em, Consts.MOTION_DETECTION_ALERT, 1) }),
            Pair("Motion detection off", { _ -> postEvent(em, Consts.MOTION_DETECTION_ALERT, 0) })
    ))
}

fun postEvent(em: EventManagerInterface, alertType: String, on: Int) {
    em.SendEvent(Event(Consts.SECURITY_EVENT_ID, "$alertType$on"))
}