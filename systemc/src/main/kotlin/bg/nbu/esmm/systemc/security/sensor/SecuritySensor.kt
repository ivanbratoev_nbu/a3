package bg.nbu.esmm.systemc.security.sensor

import Commander
import Event
import EventManagerInterface
import bg.nbu.esmm.systemc.Consts
import bg.nbu.esmm.systemc.handleMaintenancePollEvent

object Global {
    const val name = "Security sensor"
    const val description = "Detects intrusion"
}

fun main(args: Array<String>) {
    val ip = args.getOrNull(0)
    val em = if (ip == null) EventManagerInterface() else EventManagerInterface(ip)

    drawUi(em)

    var listening = true
    while (listening) {
        val eventQueue = try {
            em.GetEventQueue()
        } catch (e: Exception) {
            println("Error getting event queue: $e"); null
        }
        eventQueue?.let { queue ->
            repeat(queue.GetSize()) {

                val event = queue.GetEvent()
                val id = event.GetEventId()
                handleMaintenancePollEvent(em, id, Global.name, Global.description)
                if (id == Consts.END_EVENT_ID) {
                    try {
                        em.UnRegister()
                    } catch (e: Exception) {
                        println("Error unregistering: $e")
                    }
                    println("\n\nSimulation stopped\n\n")
                    listening = false
                }
            }
        }
        Thread.sleep(Consts.DELAY)
    }
}

fun drawUi(em: EventManagerInterface) {
    Commander("Trigger security events", Pair(300, 200), Pair(100, 200), arrayOf(
            Pair("Window break on", { _ -> postEvent(em, Consts.WINDOW_BREAK_ALERT, 1) }),
            Pair("Window break off", { _ -> postEvent(em, Consts.WINDOW_BREAK_ALERT, 0) }),
            Pair("Door break on", { _ -> postEvent(em, Consts.DOOR_BREAK_ALERT, 1) }),
            Pair("Door break off", { _ -> postEvent(em, Consts.DOOR_BREAK_ALERT, 0) }),
            Pair("Motion detection on", { _ -> postEvent(em, Consts.MOTION_DETECTION_ALERT, 1) }),
            Pair("Motion detection off", { _ -> postEvent(em, Consts.MOTION_DETECTION_ALERT, 0) })
    ))
}

fun postEvent(em: EventManagerInterface, alertType: String, on: Int) {
    em.SendEvent(Event(Consts.SECURITY_EVENT_ID, "$alertType$on"))
}