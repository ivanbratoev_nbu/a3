package bg.nbu.esmm.systemc.maintenance

import Commander
import Event
import EventManagerInterface
import MessageWindow
import bg.nbu.esmm.systemc.Consts

object Global {
    //last contact
    val devicesMap: HashMap<String, Pair<String, Long>> = HashMap()
}

fun main(args: Array<String>) {
    val ip = args.getOrNull(0)
    val em = if (ip == null) EventManagerInterface() else EventManagerInterface(ip)

    val (console, listCommander) = drawUi()
    getEventMessages(em, console, listCommander).forEach { msg ->
        val (name, message) = msg.split(";", limit = 2)
        Global.devicesMap[name] = Pair(message, System.currentTimeMillis())
    }
}

fun cleanup(console: MessageWindow) {
    val toDelete = Global.devicesMap.toList()
            //the more the devices the more time it takes for them to respond all, so using the size of the map to reflect that
            .filter { (_, value) -> val (_, lastHeardOf) = value; lastHeardOf + 3 * (Global.devicesMap.size) * Consts.DELAY < System.currentTimeMillis() }
            .map { (name, _) -> name }
    toDelete.forEach { device ->
        run {
            console.WriteMessage("Device removed: $device")
            Global.devicesMap.remove(device)
        }
    }
}

fun getEventMessages(em: EventManagerInterface, console: MessageWindow, listCommander: Commander): Sequence<String> {
    var listening = true
    return sequence {
        while (listening) {
            em.SendEvent(Event(Consts.MAINTENANCE_POLL_ID))
            val eventQueue = try {
                em.GetEventQueue()
            } catch (e: Exception) {
                console.WriteMessage("Error getting event queue: $e"); null
            }
            eventQueue?.let { queue ->
                repeat(queue.GetSize()) {
                    val event = queue.GetEvent()
                    val id = event.GetEventId()
                    if (id == Consts.END_EVENT_ID) {
                        try {
                            em.UnRegister()
                        } catch (e: Exception) {
                            console.WriteMessage("Error unregistering: $e")
                        }
                        console.WriteMessage("\n\nSimulation stopped\n\n")
                        listCommander.dispose()
                        listening = false
                    } else {
                        if (id == Consts.MAINTENANCE_POLL_REPORT_ID) {
                            yield(event.GetMessage())
                        }
                    }
                }
            }
            Thread.sleep(Consts.DELAY)
            cleanup(console)
        }
    }
}

fun drawUi(): Pair<MessageWindow, Commander> {
    val console = MessageWindow("Monitor Console", 0.40f, 0.20f)
    val listCommander = Commander("Devices commands", Pair(100, 80), Pair(800, 50), arrayOf(
            Pair("List", { _ -> listDevices(console) })
    ))
    return Pair(console, listCommander)
}

fun listDevices(console: MessageWindow) {
    console.WriteMessage("Listing all devices:")
    for ((name, value) in Global.devicesMap) {
        val (description, _) = value
        console.WriteMessage("$name: $description")
    }
    console.WriteMessage("=========END=======")
}