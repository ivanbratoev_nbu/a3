package bg.nbu.esmm.systemc.fire.controllers

import Event
import EventManagerInterface
import Indicator
import bg.nbu.esmm.systemc.Consts
import bg.nbu.esmm.systemc.handleMaintenancePollEvent

private object FireGlobal {
    const val name = "Fire alarm controller"
    const val description = "Turns the fire alarm on and off"
}

fun main(args: Array<String>) {
    val ip = args.getOrNull(0)
    val em = if (ip == null) EventManagerInterface() else EventManagerInterface(ip)

    val indicator = Indicator("Fire alarm OFF", 600, 50)

    var listening = true
    while (listening) {
        val eventQueue = try {
            em.GetEventQueue()
        } catch (e: Exception) {
            println("Error getting event queue: $e"); null
        }
        eventQueue?.let { queue ->
            repeat(queue.GetSize()) {
                val event = queue.GetEvent()
                val id = event.GetEventId()
                handleMaintenancePollEvent(em, id, FireGlobal.name, FireGlobal.description)
                if (id == Consts.END_EVENT_ID) {
                    try {
                        em.UnRegister()
                    } catch (e: Exception) {
                        println("Error unregistering: $e")
                    }
                    println("\n\nSimulation stopped\n\n")
                    indicator.dispose()
                    listening = false
                } else {
                    if (id == Consts.FIRE_EVENT_ID) {
                        when {
                            event.GetMessage().equals(Consts.FIRE_ALARM, ignoreCase = true) -> {
                                indicator.SetLampColorAndMessage("Fire alarm ON", 3)
                                em.SendEvent(Event(Consts.FIRE_EVENT_CONFIRMED_ID, Consts.FIRE_ALARM))
                            }
                            event.GetMessage().equals(Consts.FIRE_EXTINGUISHED, ignoreCase = true) -> {
                                indicator.SetLampColorAndMessage("Fire alarm OFF", 0)
                            }
                        }
                    }
                }
            }
        }
        Thread.sleep(Consts.DELAY)
    }
}
