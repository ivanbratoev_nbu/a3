package bg.nbu.esmm.systemc.fire.sensor

import Commander
import Event
import EventManagerInterface
import bg.nbu.esmm.systemc.Consts
import bg.nbu.esmm.systemc.handleMaintenancePollEvent

object Global {
    const val name = "Fire sensor"
    const val description = "Detects fire"
}

fun main(args: Array<String>) {
    val ip = args.getOrNull(0)
    val em = if (ip == null) EventManagerInterface() else EventManagerInterface(ip)

    drawUi(em)

    var listening = true
    while (listening) {
        val eventQueue = try {
            em.GetEventQueue()
        } catch (e: Exception) {
            println("Error getting event queue: $e"); null
        }
        eventQueue?.let { queue ->
            repeat(queue.GetSize()) {

                val event = queue.GetEvent()
                val id = event.GetEventId()
                handleMaintenancePollEvent(em, id, Global.name, Global.description)
                if (id == Consts.END_EVENT_ID) {
                    try {
                        em.UnRegister()
                    } catch (e: Exception) {
                        println("Error unregistering: $e")
                    }
                    println("\n\nSimulation stopped\n\n")
                    listening = false
                }
            }
        }
        Thread.sleep(Consts.DELAY)
    }
}

fun drawUi(em: EventManagerInterface) {
    Commander("Trigger fire", Pair(180, 80), Pair(400, 50), arrayOf(
            Pair("Burn everything", { _ -> postEvent(em, Consts.FIRE_ALARM) })
    ))
}

fun postEvent(em: EventManagerInterface, alertType: String) {
    em.SendEvent(Event(Consts.FIRE_EVENT_ID, alertType))
}
