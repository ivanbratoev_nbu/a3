package bg.nbu.esmm.systemc.fire.controllers

import Event
import EventManagerInterface
import Indicator
import bg.nbu.esmm.systemc.Consts
import bg.nbu.esmm.systemc.handleMaintenancePollEvent
import java.util.*
import kotlin.concurrent.schedule

private object SprinklerGlobal {
    const val name = "Sprinkler controller"
    const val description = "Turns the anti-fire sprinkler on and off"
}

fun main(args: Array<String>) {
    val ip = args.getOrNull(0)
    val em = if (ip == null) EventManagerInterface() else EventManagerInterface(ip)

    val indicator = Indicator("Sprinkler OFF", 600, 200)

    var listening = true
    while (listening) {
        val eventQueue = try {
            em.GetEventQueue()
        } catch (e: Exception) {
            println("Error getting event queue: $e"); null
        }
        eventQueue?.let { queue ->
            repeat(queue.GetSize()) {
                val event = queue.GetEvent()
                val id = event.GetEventId()
                handleMaintenancePollEvent(em, id, SprinklerGlobal.name, SprinklerGlobal.description)
                if (id == Consts.END_EVENT_ID) {
                    try {
                        em.UnRegister()
                    } catch (e: Exception) {
                        println("Error unregistering: $e")
                    }
                    println("\n\nSimulation stopped\n\n")
                    indicator.dispose()
                    listening = false
                } else {
                    if (id == Consts.FIRE_EVENT_ID) {
                        if (event.GetMessage().equals(Consts.START_SPRINKLER, ignoreCase = true)) {
                            indicator.SetLampColorAndMessage("Sprinkler ON", 3)
                            Timer().schedule(5000L) {
                                indicator.SetLampColorAndMessage("Sprinkler OFF", 0)
                                em.SendEvent(Event(Consts.FIRE_EVENT_ID, Consts.FIRE_EXTINGUISHED))
                                em.SendEvent(Event(Consts.FIRE_EVENT_CONFIRMED_ID, Consts.FIRE_EXTINGUISHED))
                            }
                        }
                    }
                }
            }
        }
        Thread.sleep(Consts.DELAY)
    }
}
