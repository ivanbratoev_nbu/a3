package bg.nbu.esmm.systemc.monitor

import Commander
import Event
import EventManagerInterface
import MessageWindow
import bg.nbu.esmm.systemc.Consts
import bg.nbu.esmm.systemc.handleMaintenancePollEvent
import java.util.*
import kotlin.concurrent.schedule

object Global {
    var securityArmed = false
    var sprinklerSchedule: TimerTask? = null
    const val name = "Monitor"
    const val description = "Displays information about the security and fire systems"
}

fun main(args: Array<String>) {
    val ip = args.getOrNull(0)
    val em = if (ip == null) EventManagerInterface() else EventManagerInterface(ip)
    val (console, armDisarmCommander) = drawUi(em)
    getEventMessages(em, console, armDisarmCommander).forEach { (id, msg) ->
        when (id) {
            Consts.SECURITY_EVENT_CONFIRMED_ID -> handleSecurityEvent(msg, console)
            Consts.FIRE_EVENT_CONFIRMED_ID -> handleFireEvent(msg, em, console)
        }
    }
}

fun handleFireEvent(msg: String, em: EventManagerInterface, console: MessageWindow) {
    when {
        msg.equals(Consts.FIRE_ALARM, ignoreCase = true) -> scheduleSprinkler(em, console)
        msg.equals(Consts.FIRE_EXTINGUISHED, ignoreCase = true) -> console.WriteMessage("Fire extinguished! Alarm and sprinklers are off")
    }
}

fun handleSecurityEvent(msg: String, console: MessageWindow) {
    if (Global.securityArmed) {
        val alertType = when {
            msg.startsWith(Consts.WINDOW_BREAK_ALERT, ignoreCase = true) -> "Window break"
            msg.startsWith(Consts.DOOR_BREAK_ALERT, ignoreCase = true) -> "Door break"
            msg.startsWith(Consts.MOTION_DETECTION_ALERT, ignoreCase = true) -> "Motion detection"
            else -> null
        }
        val state = if (msg.last() == '1') {
            "ON"
        } else {
            "OFF"
        }
        alertType?.let { alert ->
            console.WriteMessage("$alert alert is $state")
        }
    }
}

fun getEventMessages(em: EventManagerInterface, console: MessageWindow, armDisarmCommander: Commander): Sequence<Pair<Int, String>> {
    var listening = true
    return sequence {
        while (listening) {
            val eventQueue = try {
                em.GetEventQueue()
            } catch (e: Exception) {
                console.WriteMessage("Error getting event queue: $e"); null
            }
            eventQueue?.let { queue ->
                repeat(queue.GetSize()) {
                    val event = queue.GetEvent()
                    val id = event.GetEventId()
                    handleMaintenancePollEvent(em, id, Global.name, Global.description)
                    if (id == Consts.END_EVENT_ID) {
                        try {
                            em.UnRegister()
                        } catch (e: Exception) {
                            console.WriteMessage("Error unregistering: $e")
                        }
                        console.WriteMessage("\n\nSimulation stopped\n\n")
                        armDisarmCommander.dispose()
                        listening = false
                    } else {
                        yield(Pair(id, event.GetMessage()))
                        Thread.sleep(Consts.DELAY)
                    }
                }
            }
        }
    }
}

fun drawUi(em: EventManagerInterface): Pair<MessageWindow, Commander> {
    val console = MessageWindow("Monitor Console", 0.0f, 0.40f)
    val armDisarmCommander = Commander("Arm/Disarm security system", Pair(300, 150), Pair(100, 50), arrayOf(
            Pair("ARM security", { _ -> Global.securityArmed = true }),
            Pair("DISARM security", { _ -> Global.securityArmed = false }),
            Pair("Confirm sprinkler action", { _ -> confirmSprinkler(em, console) }),
            Pair("Cancel sprinkler action", { _ -> cancelSprinkler(console) })
    ))
    return Pair(console, armDisarmCommander)
}

fun scheduleSprinkler(em: EventManagerInterface, console: MessageWindow) {
    if (Global.sprinklerSchedule != null) {
        console.WriteMessage("Fire alarm detected, previous confirmation for sprinklers pending!")
    } else {
        Global.sprinklerSchedule = Timer().schedule(15000L) {
            startSprinkler(em, console)
            Global.sprinklerSchedule = null
        }
        console.WriteMessage("Fire alarm detected! You have 15 seconds to confirm or cancel the sprinklers")
    }

}

fun startSprinkler(em: EventManagerInterface, console: MessageWindow) {
    em.SendEvent(Event(Consts.FIRE_EVENT_ID, Consts.START_SPRINKLER))
    console.WriteMessage("Initiating sprinklers!")
}

fun cancelSprinkler(console: MessageWindow) {
    Global.sprinklerSchedule?.cancel()
    Global.sprinklerSchedule = null
    console.WriteMessage("Sprinkler initiation cancelled!")
}

fun confirmSprinkler(em: EventManagerInterface, console: MessageWindow) {
    cancelSprinkler(console)
    startSprinkler(em, console)
}