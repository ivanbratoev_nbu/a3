import java.awt.FlowLayout
import java.awt.LayoutManager
import java.awt.event.ActionEvent
import javax.swing.JButton
import javax.swing.JFrame

class Commander(title: String, size: Pair<Int, Int>, location: Pair<Int, Int>, buttons: Array<Pair<String, (ActionEvent) -> Unit>>) : JFrame() {
    init {
        layout = FlowLayout() as LayoutManager?

        val (width, height) = size
        val (x, y) = location

        buttons.forEach { (name, close) ->
            add(run {
                val button = JButton(name)
                button.addActionListener(close)
                button
            })
        }
        setTitle(title)
        defaultCloseOperation = JFrame.EXIT_ON_CLOSE
        setSize(width, height)
        setLocation(x, y)
        //setLocationRelativeTo(null)
        isVisible = true
        repaint()
    }

}

fun main(args: Array<String>) {
    Commander("Test", Pair(300, 100), Pair(100, 100), arrayOf(
            Pair("close", { _ -> System.exit(0) }),
            Pair("close1", { _ -> System.exit(0) }),
            Pair("close2", { _ -> System.exit(0) }),
            Pair("closeveryclose", { _ -> System.exit(0) }),
            Pair("closebloodyclose", { _ -> System.exit(0) })
    ))
}